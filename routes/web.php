<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::group([
    'middleware' => ['auth'],

], function () {
    Route::get('/', 'Admin\DashboardController@index')->name('admin');

    //Aspirasi
    Route::get('/aspirasi', 'Admin\AspirasiController@index')->name('admin.aspirasi');
    Route::get('/aspirasi/trash', 'Admin\AspirasiController@trash')->name('admin.aspirasi.trash');
    Route::post('/aspirasi/addToTrash', 'Admin\AspirasiController@addToTrash')->name('admin.aspirasi.addToTrash');
    Route::post('/aspirasi/update', 'Admin\AspirasiController@update')->name('admin.aspirasi.update');
    Route::get('/aspirasi/destroy/{id}', 'Admin\AspirasiController@destroy')->name('admin.aspirasi.destroy');

    //Jenis Aspirasi
    Route::get('/aspirasi/jenis', 'Admin\JenisAspirasiController@index')->name('admin.aspirasi.jenis');
    Route::get('/aspirasi/jenis/create', 'Admin\JenisAspirasiController@create')->name('admin.aspirasi.jenis.create');
    Route::post('/aspirasi/jenis/store', 'Admin\JenisAspirasiController@store')->name('admin.aspirasi.jenis.store');
    Route::get('/aspirasi/jenis/edit/{id}', 'Admin\JenisAspirasiController@edit')->name('admin.aspirasi.jenis.edit');
    Route::post('/aspirasi/jenis/update', 'Admin\JenisAspirasiController@update')->name('admin.aspirasi.jenis.update');
    Route::get('/aspirasi/jenis/destroy/{id}', 'Admin\JenisAspirasiController@destroy')->name('admin.aspirasi.jenis.destroy');

    //Ormawa
    Route::get('/ormawa', 'Admin\OrmawaController@index')->name('admin.ormawa');
    Route::get('/ormawa/create', 'Admin\OrmawaController@create')->name('admin.ormawa.create');
    Route::post('/ormawa/store', 'Admin\OrmawaController@store')->name('admin.ormawa.store');
    Route::get('/ormawa/edit/{id}', 'Admin\OrmawaController@edit')->name('admin.ormawa.edit');
    Route::post('/ormawa/update', 'Admin\OrmawaController@update')->name('admin.ormawa.update');
    Route::get('/ormawa/destroy/{id}', 'Admin\OrmawaController@destroy')->name('admin.ormawa.destroy');

    //Audit
    Route::get('/audit', 'Admin\AuditController@index')->name('admin.audit');
    Route::get('/audit/create', 'Admin\AuditController@create')->name('admin.audit.create');
    Route::post('/audit/store', 'Admin\AuditController@store')->name('admin.audit.store');
    Route::get('/audit/edit/{id}', 'Admin\AuditController@edit')->name('admin.audit.edit');
    Route::post('/audit/update', 'Admin\AuditController@update')->name('admin.audit.update');
    Route::get('/audit/destroy/{id}', 'Admin\AuditController@destroy')->name('admin.audit.destroy');

    //Kegiatan
    Route::get('/kegiatan', 'Admin\KegiatanController@index')->name('admin.kegiatan');
    Route::get('/kegiatan/create', 'Admin\KegiatanController@create')->name('admin.kegiatan.create');
    Route::post('/kegiatan/store', 'Admin\KegiatanController@store')->name('admin.kegiatan.store');
    Route::get('/kegiatan/edit/{id}', 'Admin\KegiatanController@edit')->name('admin.kegiatan.edit');
    Route::post('/kegiatan/update', 'Admin\KegiatanController@update')->name('admin.kegiatan.update');
    Route::get('/kegiatan/destroy/{id}', 'Admin\KegiatanController@destroy')->name('admin.kegiatan.destroy');
    Route::get('/kegiatan/verifikasi', 'Admin\KegiatanController@verifikasi')->name('admin.kegiatan.verifikasi');
    Route::post('/kegiatan/verifikasi/update', 'Admin\KegiatanController@verifikasiUpdate')->name('admin.kegiatan.verifikasi.update');

    //Blog
    Route::get('/blog', 'Admin\BlogController@index')->name('admin.blog');
    Route::get('/blog/create', 'Admin\BlogController@create')->name('admin.blog.create');
    Route::post('/blog/store', 'Admin\BlogController@store')->name('admin.blog.store');
    Route::get('/blog/edit/{id}', 'Admin\BlogController@edit')->name('admin.blog.edit');
    Route::post('/blog/update', 'Admin\BlogController@update')->name('admin.blog.update');
    Route::get('/blog/destroy/{id}', 'Admin\BlogController@destroy')->name('admin.blog.destroy');

    //Pengumuman
    Route::get('/pengumuman', 'Admin\PengumumanController@index')->name('admin.pengumuman');
    Route::get('/pengumuman/create', 'Admin\PengumumanController@create')->name('admin.pengumuman.create');
    Route::post('/pengumuman/store', 'Admin\PengumumanController@store')->name('admin.pengumuman.store');
    Route::get('/pengumuman/edit/{id}', 'Admin\PengumumanController@edit')->name('admin.pengumuman.edit');
    Route::post('/pengumuman/update', 'Admin\PengumumanController@update')->name('admin.pengumuman.update');
    Route::get('/pengumuman/destroy/{id}', 'Admin\PengumumanController@destroy')->name('admin.pengumuman.destroy');

    //Faq
    Route::get('/faq', 'Admin\FaqController@index')->name('admin.faq');
    Route::get('/faq/create', 'Admin\FaqController@create')->name('admin.faq.create');
    Route::post('/faq/store', 'Admin\FaqController@store')->name('admin.faq.store');
    Route::get('/faq/edit/{id}', 'Admin\FaqController@edit')->name('admin.faq.edit');
    Route::post('/faq/update', 'Admin\FaqController@update')->name('admin.faq.update');
    Route::get('/faq/destroy/{id}', 'Admin\FaqController@destroy')->name('admin.faq.destroy');

    //User
    Route::get('/user', 'Admin\UserController@index')->name('admin.user');
    Route::get('/user/create', 'Admin\UserController@create')->name('admin.user.create');
    Route::post('/user/store', 'Admin\UserController@store')->name('admin.user.store');
    Route::get('/user/edit/{id}', 'Admin\UserController@edit')->name('admin.user.edit');
    Route::post('/user/update', 'Admin\UserController@update')->name('admin.user.update');
    Route::get('/user/destroy/{id}', 'Admin\UserController@destroy')->name('admin.user.destroy');
});

