@extends('layouts.backend')


@section('content')
@if ($form_type=="create")
<form action="{{ route('admin.kegiatan.store')}}" method="POST" enctype="multipart/form-data">
    @else
    <form action="{{ route('admin.kegiatan.update')}}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="id" value="{{ $data['id']}}">
        @endif
        @csrf
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Tambah Data Ormawa</h4>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Nama Kegiatan</label>
                                    <input type="text" name="nama_kegiatan" value="{{ @$data['nama_kegiatan']}}"
                                        class="form-control  @error('nama_kegiatan') is-invalid @enderror"
                                        placeholder="">
                                    @error('nama_kegiatan')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                    <i class="form-group__bar"></i>
                                </div>
                                <div class="form-group">
                                    <label>Ormawa</label>
                                    <div class="select">
                                        <select name="id_ormawa"
                                            class="form-control  @error('id_ormawa') is-invalid @enderror">
                                            <option selected value="{{@$data['id_ormawa']}}">
                                                {{@$data['getOrmawa']['nama']}}</option>
                                            @foreach ($ormawa as $item)
                                            <option value="{{$item->id}}">{{$item->nama}}</option>
                                            @endforeach
                                        </select>
                                        @error('id_ormawa')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Jenis</label>
                                    <div class="select">
                                        <select name="jenis" class="form-control  @error('jenis') is-invalid @enderror">
                                            @if ($form_type=="create")
                                            <option selected disabled>Pilih jenis</option>
                                            <option value="Pendampingan">Pendampingan</option>
                                            @elseif ($form_type=="edit")
                                            <option selected value="{{@$data['jenis']}}">{{@$data['jenis']}}</option>
                                            <option value="Audiensi">Audiensi</option>
                                            <option value="Disposisi">Disposisi</option>
                                            <option value="Evaluasi">Evaluasi</option>
                                            <option value="Pendampingan">Pendampingan</option>
                                            @endif
                                        </select>
                                        @error('jenis')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Tanggal Kegiatan</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="zmdi zmdi-calendar"></i></span>
                                        </div>
                                        <input type="datetime-local" class="form-control hidden-md-up"
                                            placeholder="Pick a date &amp; time">
                                        <input type="text" name="tanggal_kegiatan"
                                            value="{{@$data['tanggal_kegiatan']}}"
                                            class="form-control  @error('tanggal_kegiatan') is-invalid @enderror datetime-picker hidden-sm-down flatpickr-input"
                                            placeholder="Pick a date &amp; time" readonly="readonly">
                                        @error('tanggal_kegiatan')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Nama Penganggung Jawab</label>
                                    <input type="text" name="nama" value="{{ @$data['nama']}}"
                                        class="form-control  @error('nama') is-invalid @enderror" placeholder="">
                                    @error('nama')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                    <i class="form-group__bar"></i>
                                </div>
                                <div class="form-group">
                                    <label>Email Penganggung Jawab</label>
                                    <input type="text" name="email" value="{{ @$data['email']}}"
                                        class="form-control  @error('email') is-invalid @enderror" placeholder="">
                                    @error('email')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                    <i class="form-group__bar"></i>
                                </div>
                                <div class="form-group">
                                    <label>Telepon Penganggung Jawab</label>
                                    <input type="text" name="telp" value="{{ @$data['telp']}}"
                                        class="form-control  @error('telp') is-invalid @enderror" placeholder="">
                                    @error('telp')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                    <i class="form-group__bar"></i>
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <div class="select">
                                        <select name="status"
                                            class="form-control  @error('status') is-invalid @enderror">
                                            @if ($form_type=="edit")
                                            <option selected value="{{@$data['status']}}">
                                                @if (@$data['status']==1)
                                                Menunggu Konfirmasi
                                                @elseif(@$data['status']==2)
                                                Diterima
                                                @elseif(@$data['status']==0)
                                                Ditolak
                                                @endif
                                            </option>
                                            @endif
                                            <option value="1">Menunggu Konfirmasi</option>
                                            <option value="2">Diterima</option>
                                            <option value="0">Ditolak</option>
                                        </select>
                                        @error('status')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn--raised">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    @endsection
