<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Audit;
use App\Models\Ormawa;

class AuditController extends Controller
{
    public function index()
    {
        $audit=Audit::all();
        return view('backend.audit.index', compact('audit'));
    }

    public function create()
    {
        $form_type="create";
        $ormawa=Ormawa::all();
        return view('backend.audit.form', compact('form_type','ormawa'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'id_ormawa' => 'required|numeric',
            'temuan' => 'required',
        ]);

        $data = array(
			'id_ormawa' => request('id_ormawa'),
            'temuan' => request('temuan'),
		);
        Audit::create($data);

        return redirect()->route('admin.audit');
    }

    public function edit($id)
    {
        $form_type="edit";
        $data=Audit::find($id);
        $ormawa=Ormawa::all();
        return view('backend.audit.form', compact('form_type','data','ormawa'));
    }

    public function update(Request $request)
    {
        $audit=Audit::find($request->id);
        $this->validate($request,[
            'id_ormawa' => 'required|numeric',
            'temuan' => 'required',
        ]);
        $data = array(
			'id_ormawa' => request('id_ormawa'),
            'temuan' => request('temuan'),
		);

        $audit->update($data);

        return redirect()->route('admin.audit');
    }

    public function destroy($id)
    {
        $audit=Audit::find($id);
        $audit->delete();

        return redirect()->route('admin.audit');
    }
}
