<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisAspirasi extends Model
{
    use HasFactory;
    protected $table = 'jenis_aspirasi';
    protected $guarded = [];
}
