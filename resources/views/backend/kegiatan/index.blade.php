@extends('layouts.backend')

@section('content')
<div class="card">
    <div class="card-body">
        <h4 class="card-title">Data Kegiatan</h4>
        <a class="btn btn-primary btn--icon-text text-white" href="{{route('admin.kegiatan.create')}}"><i
                class="zmdi zmdi-plus"></i> Tambah Data</a>
        <a class="btn btn-primary btn--icon-text text-white"
            href="{{route('admin.kegiatan.verifikasi')}}">Verifikasi({{$jumlah}})</a>

        <div class="table-responsive">
            <table id="data-table" class="table table-bordered">
                <thead class="thead-default">
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Ormawa</th>
                        <th>Jenis</th>
                        <th>Tanggal</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($kegiatan as $i => $item)
                    <tr>
                        <td>{{$i+1}}</td>
                        <td>{{$item->nama_kegiatan}}</td>
                        <td>{{$item->getOrmawa->nama}}</td>
                        <td>{{$item->jenis}}</td>
                        <td>{{$item->tanggal_kegiatan}}</td>
                        <td>
                            <a class="btn btn-primary btn-block btn--icon-text text-white" data-toggle="modal"
                                data-target="#modal-view{{$item->id}}"><i class="zmdi zmdi-eye"></i></a>

                            @if(Route::is('admin.kegiatan.verifikasi'))
                            <form action="{{ route('admin.kegiatan.verifikasi.update')}}" method="POST" class="my-1">
                                @csrf
                                <input type="hidden" name="id" value="{{$item->id}}">
                                <input type="hidden" name="status" value="2">
                                <button class="btn btn-success btn--icon-text text-white btn-block"><i
                                        class="zmdi zmdi-check"></i></button>
                            </form>
                            <button data-toggle="modal" data-target="#modal-tolak{{$item->id}}"
                                class="btn btn-block btn-danger btn--icon-text text-white"><i
                                    class="zmdi zmdi-close"></i></button>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


{{-- Modals --}}
@foreach ($kegiatan as $item)
<div class="modal fade" id="modal-view{{$item->id}}" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title pull-left">Detail Kegiatan</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 mt-5 mt-md-0">
                        <h6 class="font-weight-normal">Nama Kegiatan</h6>
                        <h5>{{$item->nama_kegiatan}}</h5>
                        <hr>
                        <h6 class="font-weight-normal">Ormawa</h6>
                        <h5>{{$item->getOrmawa->nama}}</h5>
                        <hr>
                        <h6 class="font-weight-normal">Jenis</h6>
                        <h5>{{$item->jenis}}</h5>
                        <hr>
                        <h6 class="font-weight-normal">Tanggal</h6>
                        <h5>{{$item->tanggal_kegiatan}}</h5>
                        <hr>
                        <h6 class="font-weight-normal">Email</h6>
                        <h5>{{$item->email}}</h5>
                        <hr>
                        <h6 class="font-weight-normal">Telepon</h6>
                        <h5>{{$item->telp}}</h5>
                        <hr>
                        <h6 class="font-weight-normal">Nama</h6>
                        <h5>{{$item->nama}}</h5>
                        <hr>
                        <h6 class="font-weight-normal">Status</h6>
                        @if ($item->status==1)
                        <h5>Menunggu Konfirmasi</h5>
                        @elseif($item->status==2)
                        <h5>Diterima</h5>
                        @elseif($item->status==0)
                        <h5>Ditolak</h5>
                        @endif
                        <hr>
                        <a class="btn btn-warning btn--icon-text text-white"
                            href="{{ route('admin.kegiatan.edit', ['id'=>$item->id])}}"><i
                                class="zmdi zmdi-edit"></i></a>
                        <a class="btn btn-danger btn--icon-text text-white" data-toggle="modal"
                            data-target="#modal-delete{{$item->id}}"><i class="zmdi zmdi-delete"></i></a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-tolak{{$item->id}}" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('admin.kegiatan.verifikasi.update')}}" method="POST" class="my-1">
                <div class="modal-header">
                    <h5 class="modal-title text-center">Tolak Kegiatan</h5>
                </div>
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="id" value="{{$item->id}}">
                    <input type="hidden" name="status" value="0">
                    <div class="form-group">
                        <label>Keterangan</label>
                        <textarea name="keterangan"
                            class="form-control @error('keterangan') is-invalid @enderror"></textarea>
                        @error('keterangan')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        <i class="form-group__bar"></i>
                    </div>
                </div>
                <div class="modal-footer ">
                    <button type="submit" class="btn btn-danger btn--icon-text text-white">Tolak</button>
                    <a class="btn btn-link text-center" data-dismiss="modal">Close</a>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-delete{{$item->id}}" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center">Delete</h5>
            </div>
            <div class="modal-body">
                Hapus ini?
            </div>
            <div class="modal-footer ">
                <a href=" {{ route('admin.kegiatan.destroy', ['id'=>$item->id])}}"
                    class="btn btn-danger text-center">Delete</a>
                <a class="btn btn-link text-center" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
@endforeach

@endsection
