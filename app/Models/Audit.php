<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{
    use HasFactory;
    protected $guarded = [];

    function getOrmawa(){
		return $this->belongsTo('App\Models\Ormawa','id_ormawa');
	}
}
