@extends('layouts.backend')


@section('content')
@if ($form_type=="create")
<form action="{{ route('admin.ormawa.store')}}" method="POST" enctype="multipart/form-data">
    @else
    <form action="{{ route('admin.ormawa.update')}}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="id" value="{{ $data['id']}}">
        @endif
        @csrf
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Tambah Data Ormawa</h4>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" name="nama" value="{{ @$data['nama']}}" class="form-control  @error('nama') is-invalid @enderror"
                                        placeholder="">
                                    @error('nama')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                    <i class="form-group__bar"></i>
                                </div>

                                <div class="form-group">
                                    <label>Kepanjangan</label>
                                    <input type="text" name="kepanjangan" value="{{ @$data['kepanjangan']}}"
                                        class="form-control @error('kepanjangan') is-invalid @enderror" placeholder="">
                                    @error('kepanjangan')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                    <i class="form-group__bar"></i>
                                </div>

                                <div class="form-group">
                                    <label>Nama Ketua</label>
                                    <input type="text" name="nama_ketua" value="{{ @$data['nama_ketua']}}"
                                        class="form-control  @error('nama_ketua') is-invalid @enderror" placeholder="">
                                    @error('nama_ketua')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                    <i class="form-group__bar"></i>
                                </div>

                                <div class="form-group">
                                    <label>No Telepon ketua</label>
                                    <input type="text" name="no_telp_ketua" value="{{ @$data['no_telp_ketua']}}"
                                        class="form-control  @error('no_telp_ketua') is-invalid @enderror" placeholder="">
                                    @error('no_telp_ketua')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                    <i class="form-group__bar"></i>
                                </div>

                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Instagram</label>
                                    <input type="text" name="instagram" value="{{ @$data['instagram']}}"
                                        class="form-control  @error('instagram') is-invalid @enderror" placeholder="">
                                    @error('instagram')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                    <i class="form-group__bar"></i>
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <div class="select">
                                        <select name="status" class="form-control @error('status') is-invalid @enderror">
                                            @if ($form_type=="edit")
                                            <option value="{{ @$data['status']}}" selected>
                                                @if (@$data['status']==0)
                                                Non Aktif
                                                @elseif(@$data['status']==1)
                                                Aktif
                                                @elseif(@$data['status']==2)
                                                Beku
                                                @endif
                                            </option>
                                            @endif
                                            <option value="1">Aktif</option>
                                            <option value="2">Beku</option>
                                            <option value="0">Non Aktif</option>
                                        </select>
                                        @error('status')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>logo</label>
                                    <input type="file" name="logo" value="{{ @$data->nama}}" class="form-control  @error('logo') is-invalid @enderror"
                                        placeholder="">
                                    @error('logo')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn--raised">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    @endsection
