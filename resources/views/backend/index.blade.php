@extends('layouts.backend')

@section('content')
<div class="row quick-stats">
    <div class="col-sm-6 col-md-3">
        <div class="quick-stats__item bg-blue">
            <div class="quick-stats__info">
                <h2>{{$data['aspirasi']}}</h2>
                <small>Aspirasi</small>
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-md-3">
        <div class="quick-stats__item bg-amber">
            <div class="quick-stats__info">
                <h2>{{$data['ormawa']}}</h2>
                <small>Ormawa</small>
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-md-3">
        <div class="quick-stats__item bg-purple">
            <div class="quick-stats__info">
                <h2>{{$data['audit']}}</h2>
                <small>Audit</small>
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-md-3">
        <div class="quick-stats__item bg-red">
            <div class="quick-stats__info">
                <h2>{{$data['pengumuman']}}</h2>
                <small>Pengumuman</small>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Grafik Kegiatan</h4>
                {{-- <h6 class="card-subtitle">Commodo luctus nisi erat porttitor ligula eget lacinia odio semnec elit</h6> --}}

                <div class="flot-chart flot-line"></div>
                <div class="flot-chart-legends flot-chart-legends--line"></div>
            </div>
        </div>
    </div>
</div>
@endsection


