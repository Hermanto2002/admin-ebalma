@extends('layouts.backend')


@section('content')
@if ($form_type=="create")
<form action="{{ route('admin.faq.store')}}" method="POST" enctype="multipart/form-data">
    @else
    <form action="{{ route('admin.faq.update')}}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="id" value="{{ $data['id']}}">
        @endif
        @csrf
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Tambah Data Faq</h4>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Pertanyaan</label>
                                    <input type="text" name="pertanyaan" value="{{ @$data['pertanyaan']}}"
                                        class="form-control  @error('pertanyaan') is-invalid @enderror"
                                        placeholder="">
                                    @error('pertanyaan')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                    <i class="form-group__bar"></i>
                                </div>

                                <div class="form-group">
                                    <label>Jawaban</label>
                                    <textarea name="jawaban"
                                        class="form-control  @error('jawaban') is-invalid @enderror">{{ @$data['jawaban']}}</textarea>
                                    @error('jawaban')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn--raised">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    @endsection
