<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Kegiatan;
use App\Models\Aspirasi;
use App\Models\Ormawa;
use App\Models\Audit;
use App\Models\Pengumuman;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function index()
    {
        $data = array(
            'aspirasi' => Aspirasi::all()->count(),
            'ormawa' => Ormawa::all()->count(),
            'audit' => Audit::all()->count(),
            'pengumuman' => Pengumuman::all()->count(),
        );

        for ($i=1; $i <=12 ; $i++) {
            if ($i<=0 || $i<=9) {
                $month="0$i";
            }else{
                $month="$i";
            }

            $audiensi[$i]=Kegiatan::where('jenis', 'Audiensi')->whereMonth('created_at', '=', $month)->count();
            $disposisi[$i]=Kegiatan::where('jenis', 'Disposisi')->whereMonth('created_at', '=', $month)->count();
            $pendampingan[$i]=Kegiatan::where('jenis', 'Pendampingan')->whereMonth('created_at', '=', $month)->count();
            $evaluasi[$i]=Kegiatan::where('jenis', 'Evaluasi')->whereMonth('created_at', '=', $month)->count();
        }

        // dd($disposisi);
        $page="dashboard";
        return view('backend.index', compact('data','page','audiensi','disposisi','pendampingan','evaluasi'));
    }
}
