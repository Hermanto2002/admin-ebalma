<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pengumuman;

class PengumumanController extends Controller
{
    public function index()
    {
        $pengumuman=Pengumuman::all();
        return view('backend.pengumuman.index', compact('pengumuman'));
    }

    public function create()
    {
        $form_type="create";
        return view('backend.pengumuman.form', compact('form_type'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'judul' => 'required',
            'detail' => 'required',
        ]);
        $data = array(
			'judul' => request('judul'),
            'detail' => request('detail'),
		);

        Pengumuman::create($data);

        return redirect()->route('admin.pengumuman');
    }

    public function edit($id)
    {
        $form_type="edit";
        $data=Pengumuman::find($id);
        return view('backend.pengumuman.form', compact('form_type','data'));
    }

    public function update(Request $request)
    {
        $pengumuman=Pengumuman::find($request->id);
        $this->validate($request,[
            'judul' => 'required',
            'detail' => 'required',
        ]);
        $data = array(
			'judul' => request('judul'),
            'detail' => request('detail'),
		);

        $pengumuman->update($data);

        return redirect()->route('admin.pengumuman');
    }

    public function destroy($id)
    {
        $pengumuman=Pengumuman::find($id);
        $pengumuman->delete();

        return redirect()->route('admin.pengumuman');
    }
}
