@extends('layouts.backend')

@section('content')
<div class="card">
    <div class="card-body">
        <h4 class="card-title">Data User</h4>
        <a class="btn btn-primary btn--icon-text text-white" href="{{route('admin.user.create')}}"><i class="zmdi zmdi-plus"></i> Tambah Data</a>

        <div class="table-responsive">
            <table id="data-table" class="table table-bordered">
                <thead class="thead-default">
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Level</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($user as $i => $item)
                    <tr>
                        <td>{{$i+1}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->email}}</td>
                        <td>{{$item->level}}</td>
                        <td>
                            @if ($item->status==1)
                            Aktif
                            @elseif($item->status==0)
                            Non Aktif
                            @endif
                        </td>
                        <td>
                            <a class="btn btn-primary btn--icon-text text-white" data-toggle="modal"
                                data-target="#modal-view{{$item->id}}"><i class="zmdi zmdi-eye"></i></a>
                            <a class="btn btn-warning btn--icon-text text-white" href="{{ route('admin.user.edit', ['id'=>$item->id])}}"><i class="zmdi zmdi-edit"></i></a>
                            <a class="btn btn-danger btn--icon-text text-white" data-toggle="modal"
                                data-target="#modal-delete{{$item->id}}"><i class="zmdi zmdi-delete"></i></a>

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


{{-- Modals --}}
@foreach ($user as $item)
<div class="modal fade" id="modal-view{{$item->id}}" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title pull-left">Detail User</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4 col-12">
                        <img width="100%" src="{{ asset('images/user/'.$item->foto)}}" alt="">
                    </div>
                    <div class="col-md-8 col-12 mt-5 mt-md-0">
                        <h6 class="font-weight-normal">Nama</h6>
                        <h5>{{$item->name}}</h5>
                        <hr>
                        <h6 class="font-weight-normal">Email</h6>
                        <h5>{{$item->email}}</h5>
                        <hr>
                        <h6 class="font-weight-normal">Nama Ketua</h6>
                        <h5>{{$item->nama_ketua}}</h5>
                        <hr>
                        <h6 class="font-weight-normal">Telepon</h6>
                        <h5>{{$item->telp}}</h5>
                        <hr>
                        <h6 class="font-weight-normal">Level</h6>
                        <h5>{{$item->level}}</h5>
                        <hr>
                        <h6 class="font-weight-normal">Status</h6>
                        <h5>{{$item->status}}</h5>
                        <hr>
                        <h6 class="font-weight-normal">Jabatan</h6>
                        <h5>{{$item->jabatan}}</h5>
                        <hr>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endforeach

@foreach ($user as $item)
<div class="modal fade" id="modal-delete{{$item->id}}" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center">Delete</h5>
            </div>
            <div class="modal-body">
                Hapus ini?
            </div>
            <div class="modal-footer ">
                <a href=" {{ route('admin.user.destroy', ['id'=>$item->id])}}" class="btn btn-danger text-center">Delete</a>
                <a class="btn btn-link text-center" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
@endforeach

@endsection
