<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from byrushan.com/projects/material-admin/app/2.6/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 16 Mar 2019 07:21:48 GMT -->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Vendor styles -->
    <link rel="stylesheet"
        href="{{ asset('backend/vendors/material-design-iconic-font/css/material-design-iconic-font.min.css')}}">
    <link rel="stylesheet" href="{{ asset('backend/vendors/animate.css/animate.min.css')}}">
    <link rel="stylesheet" href="{{ asset('backend/vendors/jquery-scrollbar/jquery.scrollbar.css')}}">
    <link rel="stylesheet" href="{{ asset('backend/vendors/fullcalendar/fullcalendar.min.css')}}">
    <link rel="stylesheet" href="{{ asset('backend/vendors/flatpickr/flatpickr.min.css')}}" />

    <!-- App styles -->
    <link rel="stylesheet" href="{{ asset('backend/css/app.min.css')}}">
    <title>E-Balma</title>
</head>

<body data-ma-theme="blue">
    <main class="main">

        @include('layouts.includes._header')

        @include('layouts.includes._sidebar')

        <section class="content">


            @yield('content')

            <footer class="footer hidden-xs-down">
                <p>© Badan Legislatif Mahasiswa. All rights reserved.</p>
            </footer>
        </section>
    </main>

    <!-- Javascript -->
    <!-- Vendors -->
    <script src="{{ asset('backend/vendors/jquery/jquery.min.js')}}"></script>
    <script src="{{ asset('backend/vendors/popper.js')}}/popper.min.js')}}"></script>
    <script src="{{ asset('backend/vendors/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('backend/vendors/jquery-scrollbar/jquery.scrollbar.min.js')}}"></script>
    <script src="{{ asset('backend/vendors/jquery-scrollLock/jquery-scrollLock.min.js')}}"></script>

    <script src="{{ asset('backend/vendors/flot/jquery.flot.js')}}"></script>
    <script src="{{ asset('backend/vendors/flot/jquery.flot.resize.js')}}"></script>
    <script src="{{ asset('backend/vendors/flot.curvedlines/curvedLines.js')}}"></script>
    <script src="{{ asset('backend/vendors/jqvmap/jquery.vmap.min.js')}}"></script>
    <script src="{{ asset('backend/vendors/jqvmap/maps/jquery.vmap.world.js')}}"></script>
    <script src="{{ asset('backend/vendors/easy-pie-chart/jquery.easypiechart.min.js')}}"></script>
    <script src="{{ asset('backend/vendors/salvattore/salvattore.min.js')}}"></script>
    <script src="{{ asset('backend/vendors/sparkline/jquery.sparkline.min.js')}}"></script>
    <script src="{{ asset('backend/vendors/moment/moment.min.js')}}"></script>
    <script src="{{ asset('backend/vendors/fullcalendar/fullcalendar.min.js')}}"></script>
    <script src="{{ asset('backend/vendors/flatpickr/flatpickr.min.js')}}"></script>

    <!-- Charts and maps-->


    <!-- Vendors: Data tables -->
    <script src="{{ asset('backend/vendors/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('backend/vendors/datatables-buttons/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('backend/vendors/datatables-buttons/buttons.print.min.js')}}"></script>
    <script src="{{ asset('backend/vendors/jszip/jszip.min.js')}}"></script>
    <script src="{{ asset('backend/vendors/datatables-buttons/buttons.html5.min.js')}}"></script>


    <script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>
    <!-- App functions and actions -->
    <script src="{{ asset('backend/js/app.min.js')}}"></script>
    @if (@$page=="dashboard")
    <script>
        'use strict';

        $(document).ready(function () {

            // Chart Data
            var lineChartData = [{
                    label: 'Audiensi',
                    data: [
                        <?php
                        for($i=1;$i<=12;$i++){
                        ?>

                            [<?=$i?>, <?=$audiensi[$i]?> ],
                        <?php
                        }
                        ?>
                    ],
                    color: '#2196F3'
                },
                {
                    label: 'Disposisi',
                    data: [
                        <?php
                        for($i=1;$i<=12;$i++){
                        ?>

                            [<?=$i?>, <?=$disposisi[$i]?> ],
                        <?php
                        }
                        ?>
                    ],
                    color: '#ffc721'
                },
                {
                    label: 'Pendampingan',
                    data: [
                        <?php
                        for($i=1;$i<=12;$i++){
                        ?>

                            [<?=$i?>, <?=$pendampingan[$i]?> ],
                        <?php
                        }
                        ?>
                    ],
                    color: '#d066e2'
                },
                {
                    label: 'Evaluasi',
                    data: [
                        <?php
                        for($i=1;$i<=12;$i++){
                        ?>

                            [<?=$i?>, <?=$evaluasi[$i]?> ],
                        <?php
                        }
                        ?>
                    ],
                    color: '#ff6b68'
                },
            ];

            // Chart Options
            var lineChartOptions = {
                series: {
                    lines: {
                        show: true,
                        barWidth: 0.05,
                        fill: 0
                    }
                },
                shadowSize: 0.1,
                grid: {
                    borderWidth: 1,
                    borderColor: '#edf9fc',
                    show: true,
                    hoverable: true,
                    clickable: true
                },

                yaxis: {
                    tickColor: '#edf9fc',
                    tickDecimals: 0,
                    font: {
                        lineHeight: 13,
                        style: 'normal',
                        color: '#9f9f9f',
                    },
                    shadowSize: 0
                },

                xaxis: {
                    tickColor: '#fff',
                    tickDecimals: 0,
                    font: {
                        lineHeight: 13,
                        style: 'normal',
                        color: '#9f9f9f'
                    },
                    shadowSize: 0,
                },
                legend: {
                    container: '.flot-chart-legends--line',
                    backgroundOpacity: 0.5,
                    noColumns: 0,
                    backgroundColor: '#fff',
                    lineWidth: 0,
                    labelBoxBorderColor: '#fff'
                }
            };

            // Create chart
            if ($('.flot-line')[0]) {
                $.plot($('.flot-line'), lineChartData, lineChartOptions);
            }
        });

    </script>
    @endif
    @yield('custom-script')
</body>

<!-- Mirrored from byrushan.com/projects/material-admin/app/2.6/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 16 Mar 2019 07:22:41 GMT -->

</html>
