@extends('layouts.backend')

@section('content')
<div class="card">
    <div class="card-body">
        <h4 class="card-title">Data Blog</h4>
        <a class="btn btn-primary btn--icon-text text-white" href="{{route('admin.blog.create')}}"><i class="zmdi zmdi-plus"></i> Tambah Data</a>

        <div class="table-responsive">
            <table id="data-table" class="table table-bordered">
                <thead class="thead-default">
                    <tr>
                        <th>No</th>
                        <th>Judul</th>
                        <th>Gambar</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($blog as $i => $item)
                    <tr>
                        <td>{{$i+1}}</td>
                        <td>{{$item->judul}}</td>
                        <td width="30%"><img src="{{ asset('images/blog/'.$item->gambar)}}" width="50%" alt=""></td>
                        <td>
                            <a class="btn btn-warning btn--icon-text text-white" href="{{ route('admin.blog.edit', ['id'=>$item->id])}}"><i class="zmdi zmdi-edit"></i></a>
                            <a class="btn btn-danger btn--icon-text text-white" data-toggle="modal"
                                data-target="#modal-delete{{$item->id}}"><i class="zmdi zmdi-delete"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


{{-- Modals --}}
@foreach ($blog as $item)
<div class="modal fade" id="modal-delete{{$item->id}}" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center">Delete</h5>
            </div>
            <div class="modal-body">
                Hapus ini?
            </div>
            <div class="modal-footer ">
                <a href=" {{ route('admin.blog.destroy', ['id'=>$item->id])}}" class="btn btn-danger text-center">Delete</a>
                <a class="btn btn-link text-center" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
@endforeach

@endsection
