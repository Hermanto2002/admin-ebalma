<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\JenisAspirasi;

class JenisAspirasiController extends Controller
{
    public function index()
    {
        $jenis=JenisAspirasi::all();
        return view('backend.aspirasi.jenis.index', compact('jenis'));
    }

    public function create()
    {
        $form_type="create";
        return view('backend.aspirasi.jenis.form', compact('form_type'));
    }

    public function store(Request $request)
    {
        $data = array(
			'nama' => request('nama'),
		);


        JenisAspirasi::create($data);

        return redirect()->route('admin.aspirasi.jenis');
    }

    public function edit($id)
    {
        $form_type="edit";
        $data=JenisAspirasi::find($id);
        return view('backend.aspirasi.jenis.form', compact('form_type','data'));
    }

    public function update(Request $request)
    {
        $jenis=JenisAspirasi::find($request->id);
        $data = array(
			'nama' => request('nama'),
		);

        $jenis->update($data);

        return redirect()->route('admin.aspirasi.jenis');
    }

    public function destroy($id)
    {
        $jenis=JenisAspirasi::find($id);
        $jenis->delete();

        return redirect()->route('admin.aspirasi.jenis');
    }
}
