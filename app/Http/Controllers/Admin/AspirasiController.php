<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Aspirasi;
use App\Models\aspirasiTimeline;

class AspirasiController extends Controller
{
    public function index()
    {
        $aspirasi=Aspirasi::where('status', 1)->get();
        $aspirasiTimeline=aspirasiTimeline::all();
        return view('backend.aspirasi.index', compact('aspirasi','aspirasiTimeline'));
    }

    public function trash()
    {
        $aspirasi=Aspirasi::where('status', 0)->get();
        return view('backend.aspirasi.index', compact('aspirasi'));
    }

    public function addToTrash(Request $request)
    {
        if ($request->status==1) {
            $status=0;
        }
        elseif($request->status==0){
            $status=1;
        }

        $aspirasi=Aspirasi::find($request->id);
        $data = array(
			'status' => $status,
		);

        $aspirasi->update($data);

        return redirect()->back();
    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'aspirasi_id' => 'required|numeric',
            'status' => 'required',
            'keterangan' => 'required',
        ]);

        $data = array(
			'aspirasi_id' => request('aspirasi_id'),
			'status' => request('status'),
			'keterangan' => request('keterangan'),
		);

        aspirasiTimeline::create($data);
        return redirect()->back();
    }

    public function destroy($id)
    {
        $aspirasi=Aspirasi::find($id);
        $aspirasi->delete();

        return redirect()->back();
    }
}
