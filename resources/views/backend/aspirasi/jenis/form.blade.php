@extends('layouts.backend')


@section('content')
@if ($form_type=="create")
<form action="{{ route('admin.aspirasi.jenis.store')}}" method="POST" enctype="multipart/form-data">
    @else
    <form action="{{ route('admin.aspirasi.jenis.update')}}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="id" value="{{ $data['id']}}">
        @endif
        @csrf
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Tambah Data Jenis Aspirasi</h4>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" name="nama" value="{{ @$data['nama']}}" class="form-control"
                                        placeholder="">
                                    <i class="form-group__bar"></i>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn--raised">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    @endsection
