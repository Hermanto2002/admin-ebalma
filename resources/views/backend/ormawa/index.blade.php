@extends('layouts.backend')

@section('content')
<div class="card">
    <div class="card-body">
        <h4 class="card-title">Data Ormawa</h4>
        <a class="btn btn-primary btn--icon-text text-white" href="{{route('admin.ormawa.create')}}"><i class="zmdi zmdi-plus"></i> Tambah Data</a>

        <div class="table-responsive">
            <table id="data-table" class="table table-bordered">
                <thead class="thead-default">
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Kepanjangan</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($ormawa as $i => $item)
                    <tr>
                        <td>{{$i+1}}</td>
                        <td>{{$item->nama}}</td>
                        <td>{{$item->kepanjangan}}</td>
                        <td>
                            @if ($item->status==1)
                            Aktif
                            @elseif($item->status==0)
                            Non Aktif
                            @elseif($item->status==2)
                            Beku
                            @endif
                        </td>
                        <td>
                            <a class="btn btn-primary btn--icon-text text-white" data-toggle="modal"
                                data-target="#modal-view{{$item->id}}"><i class="zmdi zmdi-eye"></i></a>
                            <a class="btn btn-warning btn--icon-text text-white" href="{{ route('admin.ormawa.edit', ['id'=>$item->id])}}"><i class="zmdi zmdi-edit"></i></a>
                            <a class="btn btn-danger btn--icon-text text-white" data-toggle="modal"
                                data-target="#modal-delete{{$item->id}}"><i class="zmdi zmdi-delete"></i></a>

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


{{-- Modals --}}
@foreach ($ormawa as $item)
<div class="modal fade" id="modal-view{{$item->id}}" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title pull-left">Detail Ormawa</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4 col-12">
                        <img width="100%" src="{{ asset('images/ormawa/'.$item->logo)}}" alt="">
                    </div>
                    <div class="col-md-8 col-12 mt-5 mt-md-0">
                        <h6 class="font-weight-normal">Nama</h6>
                        <h5>{{$item->nama}}</h5>
                        <hr>
                        <h6 class="font-weight-normal">Kepanjangan</h6>
                        <h5>{{$item->kepanjangan}}</h5>
                        <hr>
                        <h6 class="font-weight-normal">Nama Ketua</h6>
                        <h5>{{$item->nama_ketua}}</h5>
                        <hr>
                        <h6 class="font-weight-normal">Telp Ketua</h6>
                        <h5>{{$item->no_telp_ketua}}</h5>
                        <hr>
                        <h6 class="font-weight-normal">Instagram</h6>
                        <h5>{{$item->instagram}}</h5>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endforeach

@foreach ($ormawa as $item)
<div class="modal fade" id="modal-delete{{$item->id}}" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center">Delete</h5>
            </div>
            <div class="modal-body">
                Hapus ini?
            </div>
            <div class="modal-footer ">
                <a href=" {{ route('admin.ormawa.destroy', ['id'=>$item->id])}}" class="btn btn-danger text-center">Delete</a>
                <a class="btn btn-link text-center" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
@endforeach

@endsection
