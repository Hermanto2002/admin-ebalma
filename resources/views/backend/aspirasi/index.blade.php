@extends('layouts.backend')

@section('content')
<div class="card">
    <div class="card-body">
        <h4 class="card-title">Data Aspirasi</h4>
        @if (Route::is('admin.aspirasi.trash'))
        <a class="btn btn-primary btn--icon-text text-white" href="{{route('admin.aspirasi')}}"><i
                class="zmdi zmdi-eye"></i> Aspirasi</a>
        @elseif(Route::is('admin.aspirasi'))
        <a class="btn btn-primary btn--icon-text text-white" href="{{route('admin.aspirasi.trash')}}"><i
                class="zmdi zmdi-delete"></i> Trash</a>
        <a class="btn btn-primary btn--icon-text text-white" href="{{route('admin.aspirasi.jenis')}}"><i
                class="zmdi zmdi-plus"></i> Tambah Jenis</a>
        @endif

        <div class="table-responsive">
            <table id="data-table" class="table table-bordered">
                <thead class="thead-default">
                    <tr>
                        <th>No</th>
                        <th>Nim</th>
                        <th>Detail</th>
                        <th>Tanggal Masuk</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($aspirasi as $i => $item)
                    <tr>
                        <td>{{$i+1}}</td>
                        <td>{{$item->nim}}</td>
                        <td>{{$item->detail}}</td>
                        <td>{{$item->created_at->diffForHumans()}}</td>
                        <td>
                            <a class="btn btn-warning btn--icon-text text-white" data-toggle="modal"
                                data-target="#modal-edit{{$item->id}}"><i class="zmdi zmdi-edit"></i></a>
                            <a class="btn btn-danger btn--icon-text text-white" data-toggle="modal"
                                data-target="#modal-delete{{$item->id}}"><i class="zmdi zmdi-delete"></i></a>
                            <a class="btn btn-primary btn--icon-text text-white" data-toggle="modal"
                                data-target="#modal-view{{$item->id}}"><i class="zmdi zmdi-eye"></i></a>
                            @if (Route::is('admin.aspirasi.trash'))
                            <a class="btn btn-warning btn--icon-text text-white" data-toggle="modal"
                                data-target="#modal-back{{$item->id}}"><i
                                    class="zmdi zmdi-time-restore-setting"></i></a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


{{-- Modals --}}


@foreach ($aspirasi as $item)
@if (Route::is('admin.aspirasi.trash'))
<div class="modal fade" id="modal-delete{{$item->id}}" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center">Delete</h5>
            </div>
            <div class="modal-body">
                Hapus ini?
                Data akan terhapus permanen
            </div>
            <div class="modal-footer ">
                <a href="{{ route('admin.aspirasi.destroy', ['id'=>$item->id])}}"
                    class="btn btn-danger text-center">Delete</a>
                <a class="btn btn-link text-center" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-back{{$item->id}}" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center">Kembalikan data</h5>
            </div>
            <div class="modal-body">
                Data akan dikembalikan
            </div>
            <div class="modal-footer ">
                <form action="{{ route('admin.aspirasi.addToTrash')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{$item->id}}">
                    <input type="hidden" name="status" value="{{$item->status}}">
                    <button type="submit" class="btn btn-success text-center">Kembalikan</button>
                    <a class="btn btn-link text-center" data-dismiss="modal">Close</a>
                </form>
            </div>
        </div>
    </div>
</div>
@elseif(Route::is('admin.aspirasi'))
<div class="modal fade" id="modal-delete{{$item->id}}" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center">Delete</h5>
            </div>
            <div class="modal-body">
                Hapus ini?
                Data masih dapat dikembalikan sewaktu-waktu
            </div>
            <div class="modal-footer">
                <form action="{{ route('admin.aspirasi.addToTrash')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{$item->id}}">
                    <input type="hidden" name="status" value="{{$item->status}}">
                    <button type="submit" class="btn btn-danger text-center">Delete</button>
                    <a class="btn btn-link text-center" data-dismiss="modal">Close</a>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-view{{$item->id}}" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center">History</h5>
            </div>
            <div class="modal-body">
                @foreach ($aspirasiTimeline as $data)
                @if ($data->aspirasi_id==$item->id)
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">
                            {{-- @if ($data->status==1)
                            Diajukan
                            @elseif($data->status==2)
                            Sedang Diproses
                            @elseif($data->status==3)
                            Selesai
                            @endif --}}
                            {{$data->status}}
                        </h4>
                        <h6 class="card-subtitle">{{$data->created_at->diffForHumans()}}</h6>

                        <p class="card-text">{{$data->keterangan}}</p>
                    </div>
                </div>
                @endif
                @endforeach
            </div>
            <div class="modal-footer">
                <a class="btn btn-link text-center" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-edit{{$item->id}}" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="{{ route('admin.aspirasi.update')}}" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title text-center">Edit Aspirasi</h5>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="aspirasi_id" value="{{$item->id}}">
                    <div class="form-group">
                        <label>Keterangan</label>
                        <div class="select">
                            <select name="status" class="form-control @error('status') is-invalid @enderror">
                                <option value="1">Diajukan</option>
                                <option value="2">Sedang Diproses</option>
                                <option value="3">Selesai</option>
                            </select>
                            @error('status')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                        <textarea name="keterangan" class="form-control @error('keterangan') is-invalid @enderror"></textarea>
                        @error('keterangan')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        <i class="form-group__bar"></i>
                    </div>
                </div>
                <div class="modal-footer">
                    @csrf
                    <button type="submit" class="btn btn-danger text-center">Edit</button>
                    <a class="btn btn-link text-center" data-dismiss="modal">Close</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@endforeach

@endsection
