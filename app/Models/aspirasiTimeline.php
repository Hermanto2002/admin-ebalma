<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class aspirasiTimeline extends Model
{
    use HasFactory;
    protected $table = 'aspirasi_timeline';
    protected $guarded = [];
}
