<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Kegiatan;
use App\Models\Ormawa;

class KegiatanController extends Controller
{
    public function index()
    {
        $kegiatan=Kegiatan::where('status',2)->get();
        $jumlah=Kegiatan::where('status',1)->count();
        return view('backend.kegiatan.index', compact('kegiatan','jumlah'));
    }

    public function verifikasi()
    {
        $kegiatan=Kegiatan::where('status',1)->get();
        $jumlah=Kegiatan::where('status',1)->count();
        return view('backend.kegiatan.index', compact('kegiatan','jumlah'));
    }

    public function create()
    {
        $form_type="create";
        $ormawa=Ormawa::all();
        return view('backend.kegiatan.form', compact('form_type','ormawa'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'nama_kegiatan' => 'required',
            'tanggal_kegiatan' => 'required',
            'id_ormawa' => 'required',
            'jenis' => 'required',
            'status' => 'required',
            'email' => 'required',
            'telp' => 'required',
            'nama' => 'required',
        ]);
        $data = array(
			'nama_kegiatan' => request('nama_kegiatan'),
            'tanggal_kegiatan' => request('tanggal_kegiatan'),
            'id_ormawa' => request('id_ormawa'),
            'jenis' => request('jenis'),
            'status' => request('status'),
            'email' => request('email'),
            'telp' => request('telp'),
            'nama' => request('nama')
		);

        Kegiatan::create($data);

        return redirect()->route('admin.kegiatan');
    }


    public function edit($id)
    {
        $form_type="edit";
        $data=Kegiatan::find($id);
        $ormawa=Ormawa::all();
        return view('backend.kegiatan.form', compact('form_type','data','ormawa'));
    }

    public function update(Request $request)
    {
        $kegiatan=Kegiatan::find($request->id);
        $this->validate($request,[
            'nama_kegiatan' => 'required',
            'tanggal_kegiatan' => 'required',
            'id_ormawa' => 'required',
            'jenis' => 'required',
            'status' => 'required',
            'email' => 'required',
            'telp' => 'required',
            'nama' => 'required',
        ]);
        $data = array(
			'nama_kegiatan' => request('nama_kegiatan'),
            'tanggal_kegiatan' => request('tanggal_kegiatan'),
            'id_ormawa' => request('id_ormawa'),
            'jenis' => request('jenis'),
            'status' => request('status'),
            'email' => request('email'),
            'telp' => request('telp'),
            'nama' => request('nama')
		);
        $kegiatan->update($data);
        return redirect()->route('admin.kegiatan');
    }

    public function verifikasiUpdate(Request $request)
    {
        $kegiatan=Kegiatan::find($request->id);
        $this->validate($request,[
            'status' => 'required',
            'keterangan' => 'required',
        ]);
        $data = array(
            'status' => request('status'),
            'keterangan' => request('keterangan'),
		);

        $kegiatan->update($data);

        return redirect()->route('admin.kegiatan.verifikasi');
    }

    public function destroy($id)
    {
        $kegiatan=Kegiatan::find($id);
        $kegiatan->delete();

        return redirect()->route('admin.kegiatan');
    }
}
