@extends('layouts.backend')


@section('content')
@if ($form_type=="create")
<form action="{{ route('admin.blog.store')}}" method="POST" enctype="multipart/form-data">
    @else
    <form action="{{ route('admin.blog.update')}}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="id" value="{{ $data['id']}}">
        @endif
        @csrf
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Tambah Data Blog</h4>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Judul</label>
                                    <input id="textString" type="text" name="judul" value="{{ @$data['judul']}}"
                                        class="form-control @error('judul') is-invalid @enderror" placeholder="">
                                    @error('judul')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                    <i class="form-group__bar"></i>
                                    <input id="textSlug" readonly type="text" style="width: 100%;
                                    border: none;
                                    font-size: 12px;">
                                </div>

                                <div class="form-group">
                                    <label>Detail</label>
                                    <textarea class="@error('detail') is-invalid @enderror" name="detail">{{ @$data['detail']}}</textarea>
                                    @error('detail')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Gambar</label>
                                    <input type="file" name="gambar" class="form-control @error('gambar') is-invalid @enderror">
                                    @error('gambar')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                    <i class="form-group__bar"></i>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn--raised">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    @endsection

    @section('custom-script')
    <script>
        CKEDITOR.replace('detail');

        document.getElementById("textString").addEventListener("input", function () {
            let theSlug = string_to_slug(this.value);
            document.getElementById("textSlug").value = theSlug;
        });

        function string_to_slug(str) {
            str = str.replace(/^\s+|\s+$/g, ""); // trim
            str = str.toLowerCase();

            // remove accents, swap ñ for n, etc
            var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
            var to = "aaaaeeeeiiiioooouuuunc------";
            for (var i = 0, l = from.length; i < l; i++) {
                str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
            }

            str = "balma.com/blog/" + str
                .replace(/[^a-z0-9 -]/g, "") // remove invalid chars
                .replace(/\s+/g, "-") // collapse whitespace and replace by -
                .replace(/-+/g, "-"); // collapse dashes

            return str;
        }

    </script>
    @endsection
