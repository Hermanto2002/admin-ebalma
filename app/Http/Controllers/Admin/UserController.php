<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use File;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $user=User::all();
        return view('backend.user.index', compact('user'));
    }

    public function create()
    {
        $form_type="create";
        return view('backend.user.form', compact('form_type'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'telp' => 'required',
            'level' => 'required',
            'status' => 'required',
            'jabatan' => 'required',
        ]);
        $data = array(
			'name' => request('name'),
            'email' => request('email'),
			'password' => Hash::make(request('password')),
			'telp' => request('telp'),
			'level' => request('level'),
			'status' => request('status'),
			'jabatan' => request('jabatan'),
		);

        if ($request->foto!=null) {
            $fotoName = time().'.'.$request->foto->extension();
            $request->foto->move(public_path('images/user/'), $fotoName);
            $data['foto']=$fotoName;
        }

        User::create($data);

        return redirect()->route('admin.user');
    }

    public function edit($id)
    {
        $form_type="edit";
        $data=User::find($id);
        return view('backend.user.form', compact('form_type','data'));
    }

    public function update(Request $request)
    {
        $user=User::find($request->id);
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'telp' => 'required',
            'level' => 'required',
            'status' => 'required',
            'jabatan' => 'required',
        ]);
        $data = array(
			'name' => request('name'),
            'email' => request('email'),
			'telp' => request('telp'),
			'level' => request('level'),
			'status' => request('status'),
			'jabatan' => request('jabatan'),
		);

        if ($request->password!=null) {
            $data['password']=Hash::make(request('password'));
        }

        if ($request->foto!=null) {
            $fotoPath = public_path('images/user/'.$user->foto);
            if(File::exists($fotoPath)) {
                File::delete($fotoPath);
            }

            $fotoName = time().'.'.$request->foto->extension();
            $request->foto->move(public_path('images/user/'), $fotoName);
            $data['foto']=$fotoName;

        }

        $user->update($data);

        return redirect()->route('admin.user');
    }

    public function destroy($id)
    {
        $user=User::find($id);
        $fotoPath = public_path('images/user/'.$user->foto);
        if(File::exists($fotoPath)) {
            File::delete($fotoPath);
        }
        $user->delete();

        return redirect()->route('admin.user');
    }
}
