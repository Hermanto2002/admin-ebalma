@extends('layouts.backend')


@section('content')
@if ($form_type=="create")
<form action="{{ route('admin.user.store')}}" method="POST" enctype="multipart/form-data">
    @else
    <form action="{{ route('admin.user.update')}}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="id" value="{{ $data['id']}}">
        @endif
        @csrf
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Tambah Data User</h4>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" name="name" value="{{ @$data['name']}}"
                                        class="form-control @error('name') is-invalid @enderror" placeholder="">
                                    @error('name')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                    <i class="form-group__bar"></i>
                                </div>

                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" name="email" value="{{ @$data['email']}}"
                                        class="form-control @error('email') is-invalid @enderror" placeholder="">
                                    @error('email')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                    <i class="form-group__bar"></i>
                                </div>

                                <div class="form-group">
                                    <label>Telepon</label>
                                    <input type="number" name="telp" value="{{ @$data['telp']}}"
                                        class="form-control @error('telp') is-invalid @enderror" placeholder="">
                                    @error('telp')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                    <i class="form-group__bar"></i>
                                </div>

                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" name="password" value=""
                                        class="form-control @error('password') is-invalid @enderror" placeholder="">
                                    @error('password')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                    <i class="form-group__bar"></i>
                                </div>

                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Level</label>
                                    <div class="select">
                                        <select name="level" class="form-control @error('level') is-invalid @enderror">
                                            @if ($form_type=="edit")
                                            <option value="{{ @$data['status']}}" selected>
                                                @if (@$data['status']==1)
                                                Kemahasiswaan
                                                @elseif(@$data['status']==2)
                                                Balma
                                                @endif
                                            </option>
                                            @endif
                                            <option value="1">Kemahasiswaan</option>
                                            <option value="2">Balma</option>
                                        </select>
                                        @error('level')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <div class="select">
                                        <select name="status" class="form-control @error('status') is-invalid @enderror">
                                            @if ($form_type=="edit")
                                            <option value="{{ @$data['status']}}" selected>
                                                @if (@$data['status']==0)
                                                Non Aktif
                                                @elseif(@$data['status']==1)
                                                Aktif
                                                @endif
                                            </option>
                                            @endif
                                            <option value="1">Aktif</option>
                                            <option value="0">Non Aktif</option>
                                        </select>
                                        @error('status')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Jabatan</label>
                                    <input type="text" name="jabatan" value="{{ @$data['jabatan']}}"
                                        class="form-control @error('jabatan') is-invalid @enderror" placeholder="">
                                    @error('jabatan')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                    <i class="form-group__bar"></i>
                                </div>
                                <div class="form-group">
                                    <label>Foto</label>
                                    <input type="file" name="foto"
                                        class="form-control @error('foto') is-invalid @enderror" placeholder="">
                                    @error('foto')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn--raised">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    @endsection
