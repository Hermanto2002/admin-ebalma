<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ormawa;
use File;

class OrmawaController extends Controller
{
    public function index()
    {
        $ormawa=Ormawa::all();
        return view('backend.ormawa.index', compact('ormawa'));
    }

    public function create()
    {
        $form_type="create";
        return view('backend.ormawa.form', compact('form_type'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'nama' => 'required',
            'kepanjangan' => 'required',
            'nama_ketua' => 'required',
            'no_telp_ketua' => 'required|numeric',
            'instagram' => 'required',
            'status' => 'required',
            'logo' => 'required',
        ]);

        $data = array(
			'nama' => request('nama'),
            'kepanjangan' => request('kepanjangan'),
			'nama_ketua' => request('nama_ketua'),
			'no_telp_ketua' => request('no_telp_ketua'),
			'instagram' => request('instagram'),
			'status' => request('status'),
		);

        if ($request->logo!=null) {
            $logoName = time().'.'.$request->logo->extension();
            $request->logo->move(public_path('images/ormawa/'), $logoName);
            $data['logo']=$logoName;
        }

        Ormawa::create($data);

        return redirect()->route('admin.ormawa');
    }

    public function edit($id)
    {
        $form_type="edit";
        $data=Ormawa::find($id);
        return view('backend.ormawa.form', compact('form_type','data'));
    }

    public function update(Request $request)
    {
        $ormawa=Ormawa::find($request->id);
        $this->validate($request,[
            'nama' => 'required',
            'kepanjangan' => 'required',
            'nama_ketua' => 'required',
            'no_telp_ketua' => 'required|numeric',
            'instagram' => 'required',
            'status' => 'required',
            'logo' => 'required',
        ]);
        $data = array(
			'nama' => request('nama'),
            'kepanjangan' => request('kepanjangan'),
			'nama_ketua' => request('nama_ketua'),
			'no_telp_ketua' => request('no_telp_ketua'),
			'instagram' => request('instagram'),
			'status' => request('status'),
		);

        if ($request->logo!=null) {
            $logoName = time().'.'.$request->logo->extension();
            $request->logo->move(public_path('images/ormawa/'), $logoName);
            $data['logo']=$logoName;

            $logoPath = public_path('images/ormawa/'.$ormawa->logo);
            if(File::exists($logoPath)) {
                File::delete($logoPath);
            }
        }

        $ormawa->update($data);

        return redirect()->route('admin.ormawa');
    }

    public function destroy($id)
    {
        $ormawa=Ormawa::find($id);
        $logoPath = public_path('images/ormawa/'.$ormawa->logo);
        if(File::exists($logoPath)) {
            File::delete($logoPath);
        }
        $ormawa->delete();

        return redirect()->route('admin.ormawa');
    }
}
