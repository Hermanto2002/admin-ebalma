@extends('layouts.backend')


@section('content')
@if ($form_type=="create")
<form action="{{ route('admin.pengumuman.store')}}" method="POST" enctype="multipart/form-data">
    @else
    <form action="{{ route('admin.pengumuman.update')}}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="id" value="{{ $data['id']}}">
        @endif
        @csrf
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Tambah Data Pengumuman</h4>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Judul</label>
                                    <input id="textString" type="text" name="judul" value="{{ @$data['judul']}}"
                                        class="form-control @error('judul') is-invalid @enderror" placeholder="">
                                    @error('judul')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                    <i class="form-group__bar"></i>
                                </div>

                                <div class="form-group">
                                    <label>Detail</label>
                                    <textarea name="detail"
                                        class="form-control  @error('detail') is-invalid @enderror">{{ @$data['detail']}}</textarea>
                                    @error('detail')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn--raised">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    @endsection
