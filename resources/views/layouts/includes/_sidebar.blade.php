<aside class="sidebar">
    <div class="scrollbar-inner">
        <div class="user">
            <div class="user__info" data-toggle="dropdown">
                <img class="user__img" src="@if(Auth::user()->foto==null){{ asset('images/user/user.png')}}@else {{ asset('images/user/'.Auth::user()->foto)}} @endif" alt="">
                <div>
                    <div class="user__name">{{ Auth::user()->name}}</div>
                    <div class="user__email">{{ Auth::user()->email}}</div>
                </div>
            </div>

            <div class="dropdown-menu">
                <a class="dropdown-item" href="#">View Profile</a>
                <a class="dropdown-item" href="#">Settings</a>
                <a class="dropdown-item" href="#">Logout</a>
            </div>
        </div>

        <ul class="navigation">
            <li class="@if(Route::is('admin'))navigation__active @endif"><a href="{{route('admin')}}"><i class="zmdi zmdi-home"></i>Dashboard</a></li>
            <li class="@if(Route::is('admin.aspirasi'))navigation__active @endif"><a href="{{route('admin.aspirasi')}}"><i class="zmdi zmdi-looks"></i>Aspirasi</a></li>
            <li class="@if(Route::is('admin.ormawa'))navigation__active @endif"><a href="{{route('admin.ormawa')}}"><i class="zmdi zmdi-accounts"></i>Ormawa</a></li>
            <li class="@if(Route::is('admin.audit'))navigation__active @endif"><a href="{{route('admin.audit')}}"><i class="zmdi zmdi-book"></i>Audit</a></li>
            <li class="@if(Route::is('admin.kegiatan'))navigation__active @endif"><a href="{{route('admin.kegiatan')}}"><i class="zmdi zmdi-calendar-alt"></i>Kegiatan</a></li>
            <li class="@if(Route::is('admin.blog'))navigation__active @endif"><a href="{{route('admin.blog')}}"><i class="zmdi zmdi-library"></i>Blog</a></li>
            <li class="@if(Route::is('admin.faq'))navigation__active @endif"><a href="{{route('admin.faq')}}"><i class="zmdi zmdi-assignment"></i>FAQ</a></li>
            <li class="@if(Route::is('admin.pengumuman'))navigation__active @endif"><a href="{{route('admin.pengumuman')}}"><i class="zmdi zmdi-star"></i>Pengumuman</a></li>
            <li class="@if(Route::is('admin.user'))navigation__active @endif"><a href="{{route('admin.user')}}"><i class="zmdi zmdi-account-circle"></i>User</a></li>
            <li class="@if(Route::is('admin'))navigation__active @endif"><a href="{{route('admin')}}"><i class="zmdi zmdi-file"></i>Laporan</a></li>
        </ul>
    </div>
</aside>
