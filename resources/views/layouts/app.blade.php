<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from byrushan.com/projects/material-admin/app/2.6/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 16 Mar 2019 07:24:20 GMT -->
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Vendor styles -->
        <link rel="stylesheet" href="{{ asset('backend/vendors/material-design-iconic-font/css/material-design-iconic-font.min.css')}}">
        <link rel="stylesheet" href="{{ asset('backend/vendors/animate.css/animate.min.css')}}">

        <!-- App styles -->
        <link rel="stylesheet" href="{{ asset('backend/css/app.min.css')}}">
    </head>

    <body data-ma-theme="blue">
         @yield('content')

        <!-- Javascript -->
        <!-- backend/Vendors -->
        <script src="{{ asset('backend/vendors/jquery/jquery.min.js')}}"></script>
        <script src="{{ asset('backend/vendors/popper.js/popper.min.js')}}"></script>
        <script src="{{ asset('backend/vendors/bootstrap/js/bootstrap.min.js')}}"></script>

        <!-- App functions and actions -->
        <script src="{{ asset('backend/js/app.min.js')}}"></script>
    </body>

<!-- Mirrored from byrushan.com/projects/material-admin/app/2.6/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 16 Mar 2019 07:24:20 GMT -->
</html>
