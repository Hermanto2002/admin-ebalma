<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Faq;

class FaqController extends Controller
{
    public function index()
    {
        $faq=Faq::all();
        return view('backend.faq.index', compact('faq'));
    }

    public function create()
    {
        $form_type="create";
        return view('backend.faq.form', compact('form_type'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'pertanyaan' => 'required',
            'jawaban' => 'required',
        ]);
        $data = array(
			'pertanyaan' => request('pertanyaan'),
            'jawaban' => request('jawaban'),
            'status' => 1,
		);

        Faq::create($data);

        return redirect()->route('admin.faq');
    }

    public function edit($id)
    {
        $form_type="edit";
        $data=Faq::find($id);
        return view('backend.faq.form', compact('form_type','data'));
    }

    public function update(Request $request)
    {
        $faq=Faq::find($request->id);
        $this->validate($request,[
            'pertanyaan' => 'required',
            'jawaban' => 'required',
        ]);
        $data = array(
			'pertanyaan' => request('pertanyaan'),
            'jawaban' => request('jawaban'),
            'status' => 1,
		);

        $faq->update($data);

        return redirect()->route('admin.faq');
    }

    public function destroy($id)
    {
        $faq=Faq::find($id);
        $faq->delete();

        return redirect()->route('admin.faq');
    }
}
