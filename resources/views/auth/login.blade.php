@extends('layouts.app')

@section('content')
<div class="login">

    <!-- Login -->
    <div class="login__block active" id="l-login">
        <div class="login__block__header">
            <i class="zmdi zmdi-account-circle"></i>
            Hi there! Please Sign in
        </div>

        <div class="login__block__body">
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group form-group--float form-group--centered">
                    <input type="text" class="form-control  @error('email') is-invalid @enderror"
                        value="{{ old('email') }}"  name="email">
                    <label>Email Address</label>
                    <i class="form-group__bar"></i>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group form-group--float form-group--centered">
                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password">
                    <label>Password</label>
                    <i class="form-group__bar"></i>
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <button type="submit" class="btn btn--icon login__block__btn"><i
                        class="zmdi zmdi-long-arrow-right"></i></button>
            </form>
        </div>
    </div>
</div>
@endsection
