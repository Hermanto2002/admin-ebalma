<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use Illuminate\Support\Str;
use File;

class BlogController extends Controller
{
    public function index()
    {
        $blog=Blog::all();
        return view('backend.blog.index', compact('blog'));
    }

    public function create()
    {
        $form_type="create";
        return view('backend.blog.form', compact('form_type'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'judul' => 'required',
            'detail' => 'required',
            'slug' => 'required',
            'gambar' => 'required',
        ]);
        $data = array(
			'judul' => request('judul'),
            'detail' => request('detail'),
			'slug' => Str::slug(request('judul')),
		);

        if ($request->gambar!=null) {
            $gambarName = time().'.'.$request->gambar->extension();
            $request->gambar->move(public_path('images/blog/'), $gambarName);
            $data['gambar']=$gambarName;
        }

        Blog::create($data);

        return redirect()->route('admin.blog');
    }

    public function edit($id)
    {
        $form_type="edit";
        $data=Blog::find($id);
        return view('backend.blog.form', compact('form_type','data'));
    }

    public function update(Request $request)
    {
        $blog=Blog::find($request->id);
        $this->validate($request,[
            'judul' => 'required',
            'detail' => 'required',
            'slug' => 'required',
            'gambar' => 'required',
        ]);
        $data = array(
			'judul' => request('judul'),
            'detail' => request('detail'),
		);

        if ($request->gambar!=null) {
            $gambarName = time().'.'.$request->gambar->extension();
            $request->gambar->move(public_path('images/blog/'), $gambarName);
            $data['gambar']=$gambarName;

            $gambarPath = public_path('images/blog/'.$blog->gambar);
            if(File::exists($gambarPath)) {
                File::delete($gambarPath);
            }
        }

        $blog->update($data);

        return redirect()->route('admin.blog');
    }

    public function destroy($id)
    {
        $blog=Blog::find($id);
        $gambarPath = public_path('images/blog/'.$blog->gambar);
        if(File::exists($gambarPath)) {
            File::delete($gambarPath);
        }
        $blog->delete();

        return redirect()->route('admin.blog');
    }
}
