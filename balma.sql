-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 17 Jul 2021 pada 07.58
-- Versi server: 10.4.19-MariaDB
-- Versi PHP: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `balma`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `aspirasis`
--

CREATE TABLE `aspirasis` (
  `id` int(10) NOT NULL,
  `nim` varchar(20) DEFAULT NULL,
  `detail` text NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `aspirasis`
--

INSERT INTO `aspirasis` (`id`, `nim`, `detail`, `status`, `created_at`, `updated_at`) VALUES
(3, '200030222', 'mantap', 0, '2021-06-24 11:15:43', '2021-06-27 03:41:31'),
(4, NULL, 'haloo', 1, '2021-06-27 03:45:46', '2021-06-27 03:45:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `aspirasi_timeline`
--

CREATE TABLE `aspirasi_timeline` (
  `id` int(10) NOT NULL,
  `aspirasi_id` int(10) NOT NULL,
  `status` int(10) NOT NULL,
  `keterangan` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `aspirasi_timeline`
--

INSERT INTO `aspirasi_timeline` (`id`, `aspirasi_id`, `status`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 4, 2, 'sabar ya bang', '2021-07-13 01:50:39', '2021-07-13 01:50:39'),
(2, 4, 1, 'mantap', '2021-07-16 20:16:46', '2021-07-16 20:16:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `audits`
--

CREATE TABLE `audits` (
  `id` int(10) NOT NULL,
  `id_ormawa` int(10) NOT NULL,
  `temuan` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `audits`
--

INSERT INTO `audits` (`id`, `id_ormawa`, `temuan`, `created_at`, `updated_at`) VALUES
(2, 1, 'lorem', '2021-06-24 03:17:49', '2021-06-24 03:17:49'),
(3, 1, 'test', '2021-07-16 20:12:25', '2021-07-16 20:12:25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) NOT NULL,
  `judul` varchar(191) NOT NULL,
  `detail` text NOT NULL,
  `slug` varchar(191) NOT NULL,
  `gambar` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `blogs`
--

INSERT INTO `blogs` (`id`, `judul`, `detail`, `slug`, `gambar`, `created_at`, `updated_at`) VALUES
(4, 'This Site Has All the Dummy Text You\'ll Ever Need', '<p>Here&rsquo;s what Corporate ipsum looks like:</p>\r\n\r\n<blockquote>\r\n<p>Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI.</p>\r\n</blockquote>\r\n\r\n<p>Here&rsquo;s some Batman ipsum:</p>\r\n\r\n<blockquote>\r\n<p>Hero can be anyone. Even a man knowing something as simple and reassuring as putting a coat around a young boy shoulders to let him know the world hadn&rsquo;t ended. You fight like a younger man, there&rsquo;s nothing held back. It&rsquo;s admirable, but mistaken. When their enemies were at the gates the Romans would suspend democracy and appoint one man to protect the city. It wasn&rsquo;t considered an honor, it was a public service.</p>\r\n</blockquote>\r\n\r\n<p><img alt=\"three people sitting in front of table laughing together\" src=\"https://images.unsplash.com/photo-1522202176988-66273c2fd55f?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&amp;ixlib=rb-1.2.1&amp;auto=format&amp;fit=crop&amp;w=1000&amp;q=80\" /></p>\r\n\r\n<p>Meet the Ipsums is a handy resource to have around whenever you need some filler text. Here are other useful resources in our archives to check out:</p>\r\n\r\n<ul>\r\n	<li><a href=\"https://www.webfx.com/blog/web-design/design-projects-cost/\">A site that tells you how much design projects should cost</a></li>\r\n	<li>Some useful cheat sheets for web designers</li>\r\n	<li>This site will give you endless design inspiration</li>\r\n</ul>', 'this-site-has-all-the-dummy-text-youll-ever-need', '1625031391.jpg', '2021-06-29 21:36:31', '2021-06-29 21:45:26'),
(5, 'Natum fuisset mediocrem vim te, ferri option feugait in cum, has no erat forensibus.', '<p>Natum fuisset mediocrem vim te, ferri option feugait in cum, has no erat forensibus. Sed putant saperet delicata ad, at dico consequat mei, omnes vocibus te mei. Aeque delicatissimi ad vis, sit iudicabit repudiare et. At pri prompta consulatu gubergren, ius delectus sapientem dissentiunt no. Vim ex numquam bonorum consetetur. Nec in percipit corrumpit. Duo quis congue temporibus ut, alii mollis legendos at eum, id nominavi perfecto eum.</p>\r\n\r\n<p>Pri et quod omnis definitiones. Errem animal intellegat ad vim. Hinc noster posidonium in cum, his enim cetero at, eu soluta nominati percipitur duo. Ne brute oportere usu.</p>\r\n\r\n<p>Pri consulatu deseruisse ut, brute tibique oporteat sea eu. Velit debitis et sit, ne homero nonumy duo. Eu cum offendit consectetuer definitionem, id veri habeo persecuti pro. Id vel detraxit consetetur, ei augue definiebas vim. Mel libris dolorem perfecto ex, id cum insolens phaedrum conceptam. Est velit erant fabulas ne, id nam errem saperet.</p>\r\n\r\n<p>Diam viris blandit ut vel, an has diceret molestie posidonium. Ut dicam civibus mediocritatem quo, habemus periculis qui ut. Mandamus assueverit eum ad, vis id prodesset mnesarchum reformidans. Vix decore accusata molestiae eu. Debet consul nam eu.</p>\r\n\r\n<p>Mea ut quis liber mollis. Alia sanctus suscipiantur nec ut. Suas torquatos consequuntur est id. No his zril detracto laboramus, vim prima oblique no. Natum iudico commodo eu qui, aeque referrentur mei an, quaestio intellegam persequeris vis et. Ex duo tantas molestie, in commune convenire mel, te munere graeco complectitur usu.</p>\r\n\r\n<p>Ut qui ceteros expetendis efficiantur. Cum suas errem ne, soluta labores referrentur vis in. Summo habemus in usu, detraxit senserit deseruisse ea per. Graece contentiones necessitatibus ei duo, no velit omnium civibus per, ei ullum alienum contentiones per. Cu labore diceret duo. Aperiam honestatis ex eos, verterem laboramus id duo.</p>\r\n\r\n<p>Vis ne erant fastidii, ut inani corpora intellegam has, pro no delectus expetendis persequeris. An eos quem maluisset repudiare, vis partem maiestatis ei, labitur graecis partiendo cu vim. His ut duis sonet nostrud. Quo eu sumo eripuit, duo stet persecuti et, vix ut partem suscipiantur.</p>\r\n\r\n<p>Unum postulant imperdiet eos ex, maluisset intellegam ius ex. Integre concludaturque cu qui, ne vide noster putant sea. Mei ei movet appareat efficiendi, at vis noluisse platonem iracundia. Nulla verear sit cu. Nibh periculis in eos. Ei duo clita convenire.</p>\r\n\r\n<p>Per ne virtute accumsan sententiae. Esse dolorum dolorem nec ad. Cu quidam malorum argumentum per, illum concludaturque te vis. Nec ne facilisi moderatius, eu labitur alienum recteque mel.</p>\r\n\r\n<p>Tale fabulas dolores mel eu. Mea habeo malorum at, ad pro dicit ludus sadipscing. At oratio propriae eum, nec mutat suscipiantur ex. Has decore pertinax eu, et mei movet nostrud eloquentiam, aperiri inciderint concludaturque id pro. Usu id assum iudico comprehensam, case debitis molestie in eos.</p>', 'natum-fuisset-mediocrem-vim-te-ferri-option-feugait-in-cum-has-no-erat-forensibus', '1625031737.jpg', '2021-06-29 21:42:17', '2021-06-29 21:42:17'),
(6, 'Per ne virtute accumsan sententiae.', '<p>Natum fuisset mediocrem vim te, ferri option feugait in cum, has no erat forensibus. Sed putant saperet delicata ad, at dico consequat mei, omnes vocibus te mei. Aeque delicatissimi ad vis, sit iudicabit repudiare et. At pri prompta consulatu gubergren, ius delectus sapientem dissentiunt no. Vim ex numquam bonorum consetetur. Nec in percipit corrumpit. Duo quis congue temporibus ut, alii mollis legendos at eum, id nominavi perfecto eum.</p>\r\n\r\n<p>Pri et quod omnis definitiones. Errem animal intellegat ad vim. Hinc noster posidonium in cum, his enim cetero at, eu soluta nominati percipitur duo. Ne brute oportere usu.</p>\r\n\r\n<p>Pri consulatu deseruisse ut, brute tibique oporteat sea eu. Velit debitis et sit, ne homero nonumy duo. Eu cum offendit consectetuer definitionem, id veri habeo persecuti pro. Id vel detraxit consetetur, ei augue definiebas vim. Mel libris dolorem perfecto ex, id cum insolens phaedrum conceptam. Est velit erant fabulas ne, id nam errem saperet.</p>\r\n\r\n<p>Diam viris blandit ut vel, an has diceret molestie posidonium. Ut dicam civibus mediocritatem quo, habemus periculis qui ut. Mandamus assueverit eum ad, vis id prodesset mnesarchum reformidans. Vix decore accusata molestiae eu. Debet consul nam eu.</p>\r\n\r\n<p>Mea ut quis liber mollis. Alia sanctus suscipiantur nec ut. Suas torquatos consequuntur est id. No his zril detracto laboramus, vim prima oblique no. Natum iudico commodo eu qui, aeque referrentur mei an, quaestio intellegam persequeris vis et. Ex duo tantas molestie, in commune convenire mel, te munere graeco complectitur usu.</p>\r\n\r\n<p>Ut qui ceteros expetendis efficiantur. Cum suas errem ne, soluta labores referrentur vis in. Summo habemus in usu, detraxit senserit deseruisse ea per. Graece contentiones necessitatibus ei duo, no velit omnium civibus per, ei ullum alienum contentiones per. Cu labore diceret duo. Aperiam honestatis ex eos, verterem laboramus id duo.</p>\r\n\r\n<p>Vis ne erant fastidii, ut inani corpora intellegam has, pro no delectus expetendis persequeris. An eos quem maluisset repudiare, vis partem maiestatis ei, labitur graecis partiendo cu vim. His ut duis sonet nostrud. Quo eu sumo eripuit, duo stet persecuti et, vix ut partem suscipiantur.</p>\r\n\r\n<p>Unum postulant imperdiet eos ex, maluisset intellegam ius ex. Integre concludaturque cu qui, ne vide noster putant sea. Mei ei movet appareat efficiendi, at vis noluisse platonem iracundia. Nulla verear sit cu. Nibh periculis in eos. Ei duo clita convenire.</p>\r\n\r\n<p>Per ne virtute accumsan sententiae. Esse dolorum dolorem nec ad. Cu quidam malorum argumentum per, illum concludaturque te vis. Nec ne facilisi moderatius, eu labitur alienum recteque mel.</p>\r\n\r\n<p>Tale fabulas dolores mel eu. Mea habeo malorum at, ad pro dicit ludus sadipscing. At oratio propriae eum, nec mutat suscipiantur ex. Has decore pertinax eu, et mei movet nostrud eloquentiam, aperiri inciderint concludaturque id pro. Usu id assum iudico comprehensam, case debitis molestie in eos.</p>', 'per-ne-virtute-accumsan-sententiae', '1625031763.jpg', '2021-06-29 21:42:43', '2021-06-29 21:42:43');

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `faqs`
--

CREATE TABLE `faqs` (
  `id` int(10) NOT NULL,
  `pertanyaan` varchar(200) NOT NULL,
  `jawaban` text NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `faqs`
--

INSERT INTO `faqs` (`id`, `pertanyaan`, `jawaban`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Jam operasional BALMA', '10.00 - 16.00 WITA', 1, '2021-06-29 21:43:58', '2021-06-29 21:43:58'),
(3, 'Bagaimana bentuk bumi', 'bnm,', 1, '2021-07-09 22:03:56', '2021-07-09 22:03:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_aspirasi`
--

CREATE TABLE `jenis_aspirasi` (
  `id` int(10) NOT NULL,
  `nama` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `jenis_aspirasi`
--

INSERT INTO `jenis_aspirasi` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(1, 'Test', '2021-07-13 02:50:32', '2021-07-13 02:50:32');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kegiatans`
--

CREATE TABLE `kegiatans` (
  `id` int(10) NOT NULL,
  `nama_kegiatan` varchar(191) NOT NULL,
  `tanggal_kegiatan` timestamp NULL DEFAULT NULL,
  `id_ormawa` int(10) NOT NULL,
  `jenis` varchar(191) NOT NULL,
  `status` int(10) NOT NULL,
  `email` varchar(191) NOT NULL,
  `telp` varchar(191) NOT NULL,
  `nama` varchar(191) NOT NULL,
  `keterangan` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kegiatans`
--

INSERT INTO `kegiatans` (`id`, `nama_kegiatan`, `tanggal_kegiatan`, `id_ormawa`, `jenis`, `status`, `email`, `telp`, `nama`, `keterangan`, `created_at`, `updated_at`) VALUES
(2, 'Rapat', '2021-06-25 11:18:00', 1, 'Disposisi', 0, 'test@gmail.com', '098765432', 'Herman', NULL, '2021-06-24 11:18:13', '2021-07-13 02:16:30'),
(3, 'Lorem', '2021-06-30 04:00:00', 1, 'Pendampingan', 2, 'hermantoimade45@gmail.com', '0895630687322', 'Hermanto', NULL, '2021-06-25 05:27:45', '2021-06-29 19:38:37'),
(4, 'Lorem', '2021-06-30 04:00:00', 1, 'Pendampingan', 2, 'hermantoimade45@gmail.com', '0895630687322', 'Hermanto', NULL, '2021-06-25 05:27:45', '2021-06-29 19:38:45'),
(5, 'Lorem', '2021-06-28 18:46:00', 1, 'Audiensi', 0, 'hermantoimade45@gmail.com', '089563068732', 'Hermanto', NULL, '2021-06-27 10:48:09', '2021-06-29 19:38:49'),
(6, 'Lorem', '2021-06-22 16:00:00', 1, 'Pendampingan', 0, 'triyanatira@gmail.com', '0895630687322', 'bayu', 'kamu terlalu baik buat aku', '2021-06-29 20:16:28', '2021-07-13 02:27:15'),
(7, 'Ipsum', '2021-07-01 04:00:00', 1, 'Pendampingan', 1, 'hermantoimade45@gmail.com', '089563068732', 'Hermanto', NULL, '2021-06-29 21:26:58', '2021-06-29 21:26:58');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ormawas`
--

CREATE TABLE `ormawas` (
  `id` int(10) NOT NULL,
  `nama` varchar(191) NOT NULL,
  `kepanjangan` varchar(191) NOT NULL,
  `nama_ketua` varchar(191) NOT NULL,
  `logo` varchar(191) NOT NULL,
  `no_telp_ketua` varchar(191) NOT NULL,
  `instagram` varchar(191) NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `ormawas`
--

INSERT INTO `ormawas` (`id`, `nama`, `kepanjangan`, `nama_ketua`, `logo`, `no_telp_ketua`, `instagram`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Progress', 'Programming Of Stikom Bali', 'Yudik', 'logo.png', '1234567890', 'progress.stikombali', 1, '2021-06-21 08:50:31', '2021-06-24 02:20:52'),
(4, 'Hermanto', 'qwrtyuy', 'wqewqewqe', '1625896915.jpg', '21231321', 'dasdsad', 2, '2021-07-09 22:01:55', '2021-07-09 22:01:55'),
(5, 'Mantap', 'panjang', 'dono', '1626495792.png', '21231321', 'dasdsad', 1, '2021-07-16 20:23:13', '2021-07-16 20:23:13');

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengumuman`
--

CREATE TABLE `pengumuman` (
  `id` int(10) NOT NULL,
  `judul` varchar(191) NOT NULL,
  `detail` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pengumuman`
--

INSERT INTO `pengumuman` (`id`, `judul`, `detail`, `created_at`, `updated_at`) VALUES
(1, 'ini namanya judul edit', 'dsadasdsadasd', '2021-06-24 02:04:25', '2021-06-24 02:04:35');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telp` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `telp`, `foto`, `level`, `status`, `jabatan`, `created_at`, `updated_at`) VALUES
(1, 'Admin edit', 'admin@balma.com', '$2y$12$/I1ExIWZkOvWcteSAJs2m.BW/kdynYPg1IYtwOWui8IYlori1e95y', '123456789', '1624625896.png', '1', '1', 'Admin', '2021-06-21 07:35:15', '2021-06-25 04:58:16'),
(3, 'Admin2', 'admin@admin.com', '$2y$10$wrwCLnPz4HF9TzpZAVMIDeA9VGLcTTu811wx2bjUEuyATRntXQcsa', '0895630687322', '1624625896.png', '2', '1', 'Admin', '2021-06-25 04:52:46', '2021-06-25 04:52:46');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `aspirasis`
--
ALTER TABLE `aspirasis`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `aspirasi_timeline`
--
ALTER TABLE `aspirasi_timeline`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `audits`
--
ALTER TABLE `audits`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indeks untuk tabel `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `jenis_aspirasi`
--
ALTER TABLE `jenis_aspirasi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kegiatans`
--
ALTER TABLE `kegiatans`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `ormawas`
--
ALTER TABLE `ormawas`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `aspirasis`
--
ALTER TABLE `aspirasis`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `aspirasi_timeline`
--
ALTER TABLE `aspirasi_timeline`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `audits`
--
ALTER TABLE `audits`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `jenis_aspirasi`
--
ALTER TABLE `jenis_aspirasi`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `kegiatans`
--
ALTER TABLE `kegiatans`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `ormawas`
--
ALTER TABLE `ormawas`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `pengumuman`
--
ALTER TABLE `pengumuman`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
