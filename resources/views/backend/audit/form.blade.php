@extends('layouts.backend')


@section('content')
@if ($form_type=="create")
<form action="{{ route('admin.audit.store')}}" method="POST" enctype="multipart/form-data">
    @else
    <form action="{{ route('admin.audit.update')}}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="id" value="{{ $data['id']}}">
        @endif
        @csrf
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Tambah Data Audit</h4>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Ormawa</label>
                                    <div class="select">
                                        <select name="id_ormawa" class="form-control @error('id_ormawa') is-invalid @enderror">
                                            @if ($form_type=="create")
                                            <option selected disabled>Pilih Ormawa</option>
                                            @else
                                            <option selected value="{{$data['id_ormawa']}}">
                                                {{$data['getOrmawa']['nama']}}</option>
                                            @endif
                                            @foreach ($ormawa as $item)
                                            <option value="{{$item->id}}">{{$item->nama}}</option>
                                            @endforeach
                                        </select>
                                        @error('id_ormawa')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Temuan</label>
                                    <textarea class="form-control @error('temuan') is-invalid @enderror"
                                        name="temuan">{{ @$data['temuan']}}</textarea>
                                    @error('temuan')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn--raised">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    @endsection
